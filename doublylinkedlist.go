package main

import "fmt"

type Node struct {
	data       int
	next, prev *Node
}

type List struct {
	head, tail *Node
	length     int
}

func (l *List) Push(data int) {
	currentNode := &Node{data: data}
	if l.head == nil {
		l.head = currentNode
	} else {
		l.tail.next = currentNode
		currentNode.prev = l.tail

	}
	l.tail = currentNode
	l.length++
}

func (l List) printList() {
	headNode := l.head
	for headNode != nil {
		fmt.Printf("%d ", headNode.data)
		headNode = headNode.next
	}
	fmt.Println("\n")
}
func main() {
	myList := List{}

	myList.Push(3)
	myList.Push(7)
	myList.Push(9)
	myList.Push(5)

	myList.printList()

}
