package main

import "fmt"

//Create Node
type Node struct {
	data int
	next *Node
}

type List struct {
	head *Node

	length int
}

//add node from front
func (l *List) push(data int) {
	Node := &Node{data: data}
	if l.head == nil {
		l.head = Node
	} else {
		Node.next = l.head
		l.head = Node
	}
	l.length++
}

//Print the list
func (l List) printList() {
	headNode := l.head
	for headNode != nil {
		fmt.Printf("%d ", headNode.data)

		headNode = headNode.next
	}
	fmt.Println("\n")
}

//add data from front
func (l *List) pushBack(data int) {
	Node := &Node{data: data}
	if l.head == nil {
		l.head = Node
	} else {
		headNode := l.head
		for headNode.next != nil {
			headNode = headNode.next
		}
		headNode.next = Node
	}
	l.length++
}

// delete the node
func (l *List) delete() {
	if l.head == nil {
		fmt.Println("LinkedList is empty")
	}
	l.head = l.head.next
	l.length--
	return
}

//delete a specific node
func (l *List) deleteWithValue(value int) {
	if l.head == nil {
		fmt.Println("LinkedList is empty")
	}
	if l.head.data == value {
		l.head = l.head.next
		l.length--
		return
	}
	deleteNode := l.head
	for deleteNode.next.data != value {
		deleteNode = deleteNode.next
	}
	deleteNode.next = deleteNode.next.next
	l.length--
}

func main() {
	linkedList := List{}
	linkedList.push(12)
	linkedList.push(5)
	linkedList.push(2)
	linkedList.push(1)
	linkedList.push(6)

	linkedList.printList()

	linkedList.delete()
	linkedList.printList()

	linkedList.deleteWithValue(5)
	linkedList.printList()

}
