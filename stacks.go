package main

import "fmt"

type Stack struct {
	items []int
}

//push item
func (s *Stack) Push(i int) {
	s.items = append(s.items, i)
}

//pop item
func (s *Stack) Pop() int {
	l := len(s.items) - 1
	removedItem := s.items[l]
	s.items = s.items[:l]
	return removedItem

}

func main() {
	myStack := Stack{}
	fmt.Println(myStack)
	myStack.Push(100)
	myStack.Push(200)
	myStack.Push(300)
	fmt.Println(myStack)

	myStack.Pop()
	fmt.Println(myStack)

}
